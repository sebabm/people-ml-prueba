package com.sebabm.mercadolibre;

public class Productos {
    private String imagen;
    private String title;
    private int quantity;
    private String id;
    private double precio;


    public Productos(String id, String imagen, String title, int quantity, double precio) {
        this.id = id;
        this.imagen = imagen;
        this.title = title;
        this.quantity = quantity;
        this.precio = precio;
    }

    public String getImagen() {
        return imagen;
    }

    public String getTitle() {
        return title;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getId() {
        return id;
    }

    public double getPrecio() {
        return precio;
    }
}

