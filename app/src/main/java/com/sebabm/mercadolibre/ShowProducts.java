package com.sebabm.mercadolibre;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ShowProducts extends AppCompatActivity {
    private static final String TAG = "MERCADOLIBRE";
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<String> ImagesArray = new ArrayList<>();
    private TextView TVDescripcion, TVPrecio, TVTitulo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_products);
        TVDescripcion = findViewById(R.id.tvDescripcion);
        TVPrecio = findViewById(R.id.tvPrecio);
        TVTitulo = findViewById(R.id.tvTitulo);


        //Recupero el ID del producto seleccionado
        Intent intent = getIntent();
        String idProducto = intent.getStringExtra("id_producto");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Producto: " + idProducto);

        mostrarImagenes("https://api.mercadolibre.com/items/" + idProducto);
        cargarDescripcion("https://api.mercadolibre.com/items/" + idProducto + "/descriptions");

    }


    public void cargarDescripcion(String url) {
        Log.e(TAG, url);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject oneObject = response.getJSONObject(0);
                            String descripcion = oneObject.getString("plain_text");
//                            Log.e(TAG, String.valueOf(oneObject.getString("plain_text")));
                            TVDescripcion.setText(descripcion);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace();
                    }
                });

        // Access the RequestQueue through your singleton class.
        queue.add(jsonObjectRequest);
    }


    public void mostrarImagenes(String url) {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            JSONArray array = response.getJSONArray("pictures");
                            Double precio = response.getDouble("price");
                            String titulo = response.getString("title");


//                            Log.e(TAG, String.valueOf(response));
                            TVPrecio.setText("Precio: $ " + precio);
                            TVTitulo.setText(titulo);


                            for (int i = 0; i < array.length(); i++) {
                                JSONObject oneObject = array.getJSONObject(i);
                                ImagesArray.add(oneObject.getString("url"));
//                                Log.e(TAG, oneObject.getString("url"));
                            }
                            init();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        // Access the RequestQueue through your singleton class.
        queue.add(jsonObjectRequest);
    }


    private void init() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImageAdapter(ShowProducts.this, ImagesArray));
        final float density = getResources().getDisplayMetrics().density;
        NUM_PAGES = ImagesArray.size();
    }
}
