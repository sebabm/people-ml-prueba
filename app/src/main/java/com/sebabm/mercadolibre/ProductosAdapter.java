package com.sebabm.mercadolibre;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.List;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.ProductosViewHolder> {
    private List<Productos> items;


    public static class ProductosViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public ImageView imagen;
        public TextView id;
        public TextView titulo;
        public TextView cantidad;
        public TextView precio;


        public ProductosViewHolder(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            titulo = (TextView) v.findViewById(R.id.titulo);
            cantidad = (TextView) v.findViewById(R.id.cantidad);
            precio = (TextView) v.findViewById(R.id.precio);

        }
    }

    public ProductosAdapter(List<Productos> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ProductosViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card, viewGroup, false);


        return new ProductosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ProductosViewHolder viewHolder, final int i) {
        viewHolder.titulo.setText(items.get(i).getTitle());
        viewHolder.precio.setText("Precio: $" + String.valueOf(items.get(i).getPrecio()));
        viewHolder.cantidad.setText("Cantidad disponible: " + String.valueOf(items.get(i).getQuantity()));
        new DownloadImageTask(viewHolder.imagen).execute(items.get(i).getImagen());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(view.getContext(), "Recycle Click" + items.get(i).getId(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), ShowProducts.class);
                intent.putExtra("id_producto", items.get(i).getId());
                view.getContext().startActivity(intent);

            }
        });


    }

    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}