package com.sebabm.mercadolibre;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MERCADOLIBRE";
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    List items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Principal");

        handleIntent(getIntent());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_custom, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        return true;
    }


    @Override
    protected void onNewIntent(Intent intent) {
//        Log.e(TAG,"onNewIntent");

        handleIntent(intent);
    }


    private void handleIntent(Intent intent) {
//        Log.e(TAG,"handleIntent");


        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.e(TAG, query);

            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle("Busqueda: " + query);


            String url = "https://api.mercadolibre.com/sites/MLA/search?q=" + query;
            RequestQueue queue = Volley.newRequestQueue(this);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                items = new ArrayList();

                                JSONArray array = response.getJSONArray("results");
//                                Log.e(TAG, array.toString());


                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject oneObject = array.getJSONObject(i);

                                    String id = oneObject.getString("id");
                                    String imagen = oneObject.getString("thumbnail");
                                    String titulo = oneObject.getString("title");
                                    int cantidad = oneObject.getInt("available_quantity");
                                    double precio = oneObject.getDouble("price");


//                                    Log.e(TAG, imagen);
                                    items.add(new Productos(id, imagen, titulo, cantidad, precio));

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            // Obtener el Recycler
                            recycler = (RecyclerView) findViewById(R.id.reciclador);
                            recycler.setHasFixedSize(true);

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getApplicationContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new ProductosAdapter(items);
                            recycler.setAdapter(adapter);

                            recycler.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
//                                    Log.e(TAG, "Click");
                                }
                            });


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO: Handle error

                        }
                    });

            // Access the RequestQueue through your singleton class.
            queue.add(jsonObjectRequest);


        }
    }

}
