# Mejores o agregados a la consigna
- Busqueda en action bar.
- Se muestran los resultados con una imagen, cantidad disponibles, titulo y precio.
- Al elegir un producto se abre una nueva ventana con un slide de fotos(Manual), Titulo de la publicacion, precio y la descripcion detallada.
- Al abrir un producto se muestra el ID del producto.
- Se colocaron los botones para volver a la activity anterior en el action bar.
- Se muestra en MainActivity el termino usado para buscar.
- Al momento de busqueda se muestra imagenes de baja calidad para que sea mas rapido.



# Manejo de errores 
- Usando Log de errores con Log.e
---
- En caso de cualquier tipo de error se imprime el error en consola con Log.e

# Verificacion de datos
- Usando Log (Solo usado para desarrollo)


## Dependencias
Volley para consumir las apis de  ```https://api.mercadolibre.com/```
- [Volley](https://developer.android.com/training/volley)
- Uso de Cardview y recyclerView para mostrar los resultados de la busqueda.
- 
# APK debug compilada
[APK para prueba rapida](https://gitlab.com/sebabm/people-ml-prueba/blob/master/app-debug.apk)




